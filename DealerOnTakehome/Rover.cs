﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealerOnTakeHomeKarimSalmi
{    
    public class Rover
    {
        public int xPosition { get; set; }
        public int yPosition { get; set; }
        public Direction Facing { get; set; }

        public Rover(int xPos, int yPos, Direction dir)
        {
            xPosition = xPos;
            yPosition = yPos;
            Facing = dir;
        }

        public enum Direction
        {
            North,
            East,
            South,
            West,
        }

        internal void Move(char instruction)
        {
            switch (instruction)
            {
                case 'L':
                    if (Facing > 0)
                    {
                        Facing -= 1;
                    }
                    else
                    {
                        Facing = Direction.West;
                    }
                    break;
                case 'R':
                    if (Facing == Direction.West)
                    {
                        Facing = 0;
                    }
                    else
                    {
                        Facing += 1;
                    }
                    break;
                case 'M':
                    switch (Facing)
                    {
                        case Direction.North:
                            yPosition += 1;
                            break;
                        case Direction.East:
                            xPosition += 1;
                            break;
                        case Direction.South:
                            yPosition -= 1;
                            break;
                        case Direction.West:
                            xPosition -= 1;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    throw new ArgumentException("Invalid instruction received: " + instruction);
            }
        }

        internal void printLocation()
        {
            char heading = ' ';
            switch (Facing)
            {
                case Direction.North:
                    heading = 'N';
                    break;
                case Direction.East:
                    heading = 'E';
                    break;
                case Direction.South:
                    heading = 'S';
                    break;
                case Direction.West:
                    heading = 'W';
                    break;
                default:
                    break;
            }
            Console.WriteLine("{0} {1} {2}", xPosition, yPosition, heading);
        }
    }
}
