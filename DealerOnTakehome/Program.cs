﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealerOnTakeHomeKarimSalmi
{
    class Program
    {
        static void Main()
        {
            LaunchRovers();

            // Readline to leave console window open to view inputs.
            Console.ReadLine();
        }

        static void LaunchRovers()
        {    
            // Receive plateau size input.
            string plateauSizeInput = Console.ReadLine();            

            // Check that only 2 values were provided.
            var coordinates = plateauSizeInput.Split(' ');
            if (coordinates.Count() != 2)
            {
                throw new ArgumentException("Only 2 inputs were expected.");
            }

            int areaX = 0;
            int areaY = 0;

            // Check that the 2 values were integers.            
            if (!int.TryParse(coordinates[0], out areaX) || !int.TryParse(coordinates[1], out areaY))
            {
                throw new ArgumentException("Invalid inputs received where two integers were expected.");
            }

            if (areaX < 0 || areaY < 0)
            {
                throw new ArgumentException("Coordinates of area cannot be negative.");
            }

            // Read input of mars rover location.
            bool isRunning = true;
            string validDirections = "NESW";
            string validInstructions = "LRM";

            while (isRunning)
            {
                // Read inputs for current rover.
                string initialRoverCoordinates = Console.ReadLine();
                string movementInstructions = Console.ReadLine();
                if (string.IsNullOrEmpty(initialRoverCoordinates))
                {
                    throw new ArgumentException("Empty input provided where rover coordinates expected.");
                }
                
                                
                if (string.IsNullOrEmpty(movementInstructions))
                {
                    throw new ArgumentException("Empty input provided where movement instructions expected.");
                }

                var splitCoordinates = initialRoverCoordinates.Split(' ');

                // Check for right number of inputs and length of facing direction.
                if (splitCoordinates.Count() != 3 || splitCoordinates[2].Length != 1)
                {
                    throw new ArgumentException("Incorrect input provided where X Y D rover coordinates expected.");
                }

                int xPos = 0;
                int yPos = 0;               
                char initialDirection = splitCoordinates[2].FirstOrDefault();

                // Check that inputs were valid
                if (!int.TryParse(splitCoordinates[0], out xPos) || !int.TryParse(splitCoordinates[1], out yPos) || !validDirections.Contains(initialDirection))
                {
                    throw new ArgumentException("Incorrect input provided where rover coordinates and direction expected.");
                }
                if (xPos < 0 || yPos < 0)
                {
                    throw new ArgumentException("Rover initial coordinates cannot be negative.");
                }

                // Create and move the rover appropriately.
                Rover currentRover = new Rover(xPos, yPos, (Rover.Direction)validDirections.IndexOf(initialDirection));
                for (int i = 0; i < movementInstructions.Length; i++)
                {
                    char instruction = movementInstructions[i];
                    if (validInstructions.Contains(instruction))
                    {
                        currentRover.Move(instruction);
                    }
                    else
                    {
                        throw new ArgumentException("Invalid instruction sent to Rover.");
                    }
                }

                // Check that rover stayed within bounds of plateau.
                if (areaX >= currentRover.xPosition && areaY >= currentRover.yPosition)
                {
                    currentRover.printLocation();
                }
                else
                {
                    throw new IndexOutOfRangeException("Rover left the bounds of the plateau.");
                }
            }
        }
    }
}

