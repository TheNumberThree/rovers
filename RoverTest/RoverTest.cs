﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;
using DealerOnTakeHomeKarimSalmi;

namespace RoverTest
{
    [TestClass]
    public class RoverTest
    {
        [TestMethod]
        public void RotateLeftWhenFacingE()
        {
            // Arrange            
            PrivateObject myRover = new PrivateObject(typeof(Rover), 1, 1, Rover.Direction.East);
            // Act
            myRover.Invoke("Move", 'L');
            // Assert
            Assert.AreEqual(myRover.GetProperty("Facing"), Rover.Direction.North);
        }

        [TestMethod]
        public void RotateRightWhenFacingW()
        {
            // Arrange            
            PrivateObject myRover = new PrivateObject(typeof(Rover), 1, 1, Rover.Direction.West);
            // Act
            myRover.Invoke("Move", 'R');
            // Assert
            Assert.AreEqual(myRover.GetProperty("Facing"), Rover.Direction.North);
        }
    }
}
